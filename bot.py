from httplib import BadStatusLine
from selenium import webdriver
from selenium.common.exceptions import NoAlertPresentException
from selenium.webdriver import FirefoxOptions
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.ui import WebDriverWait
from time import sleep
import logging
import os

""" Skip alerts """
def skip_possible_alerts(driver):
    try:
        while expected_conditions.alert_is_present():
            alert = driver.switch_to.alert
            alert.accept()
    except NoAlertPresentException:
        pass

    return 0


HOST = os.environ.get("WEB_SITE_HOST")
LOGIN = "support"
PASSWORD = "kohXaer1raeWei!phee:pai1jaeb,aen0"


""" Set loglevel """
logging.basicConfig(level=logging.INFO)

""" Set chrome options """
chrome_options = webdriver.ChromeOptions()
chrome_options.add_argument('--no-sandbox')
chrome_options.add_argument('--headless')
chrome_options.add_argument('--disable-dev-shm-usage')

driver = webdriver.Chrome('/opt/supbot/chromedriver', chrome_options=chrome_options)

driver.implicitly_wait(3)

try:
    while (True):
        try:
            logging.info("Open administrative panel...")
            driver.get("http://%s/admin/" % HOST)
            sleep(5)

            """ Authorization Process """
            logging.info("Authenticating...")
            driver.find_element_by_name("login").send_keys(LOGIN)
            driver.find_element_by_name("password").send_keys(PASSWORD)
            driver.find_element_by_id("submit").click()

            """ Skip alerts """
            logging.info("Skipping alerts...")
            skip_possible_alerts(driver)

            """ Checking orders"""
            logging.info("Checking orders...")
            all_checkboxes = driver.find_elements_by_name("delete[]")
            if all_checkboxes:
                sleep(15)
                for checkbox in all_checkboxes:
                    checkbox.click()

                """ Deleting orders """
                driver.find_element_by_id("submit").click()

            """ Skip alerts """
            logging.info("Skipping alerts before logout...")
            skip_possible_alerts(driver)
            sleep(1)

            """ Logouting """
            logging.info("Logouting...")
            driver.find_element_by_id("logout").click()

        except BadStatusLine as e:
            logging.error("Something is fucked up %s time" % i)
        except Exception as e:
            logging.exception(e)

except BaseException as e:
    logging.exception(e)
    driver.quit()

