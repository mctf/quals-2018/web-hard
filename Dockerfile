FROM php:7.0-apache

RUN apt-get update && apt-get -y upgrade && apt-get -y install \
    curl lynx-cur libmcrypt-dev
RUN docker-php-ext-install mysqli mcrypt mbstring pdo_mysql

RUN a2enmod headers

COPY application /var/www/public
WORKDIR /var/www/public

COPY 000-default.conf /etc/apache2/sites-available/000-default.conf

VOLUME ["/var/www/public"]

EXPOSE 80

CMD /usr/sbin/apache2ctl restart
CMD /usr/sbin/apache2ctl -D FOREGROUND
