<?php

@include('../config.php');

if(!empty($_POST['login']) && !empty($_POST['password']) && !is_array($_POST['login']) && !is_array($_POST['password'])) {

        $login = strtolower(trim($_POST['login']));

        $sql = 'SELECT * FROM users WHERE BINARY login = :login';

        $query = $dbh->prepare($sql);

        $parameters = Array(
            ':login' => $login,
        );

        $query->execute($parameters);

        $result = $query->fetch();

        if ($result) {

            if (password_verify(trim($_POST['password']), $result['password'])) {

                $userdata = Array(
                    'id' => $result['id'],
                    'name' => $result['name'],
                    'login' => $result['login'],
                    'uuid' => $result['uuid'],
                    'type' => $result['type'],
                    'status' => $result['status']
                );

                // session_regenerate_id(true);

                $_SESSION['userdata'] = $userdata;
                header('Location: /admin/index.php');
                exit();
            }
            else {

                $error = 'Login or password is incorrect';
            }
        }
        else {

            $error = 'Login or password is incorrect1';
        }
}

if ( !empty($_SESSION['userdata']) ){

    if ( !empty($_POST["delete"]) ){

      try {
        $stmt = $dbh->prepare('DELETE FROM orders WHERE id = :id');

        foreach ($_POST["delete"] as $order_id) {
          $stmt->bindValue('id', $order_id, PDO::PARAM_INT);
          $stmt->execute(); 
        }
      } catch (PDOException $e) {
         $error = 'MySQL Error';
      }
    }

    try {
      $sql = 'SELECT column_name FROM INFORMATION_SCHEMA.columns WHERE table_name="orders"';
      $columns = $dbh->query($sql);

      if ( $columns ) {
          $sql = 'SELECT * FROM orders ORDER BY id ASC LIMIT 10';
          $query = $dbh->query($sql);
          $orders = $query->fetchAll(PDO::FETCH_ASSOC);
      }
    } catch (PDOException $e) {
      $error = 'MySQL Error';
      var_dump($e);
    }
}

?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
<title>Administrator's Panel</title>
<!-- Google Analytics -->
<script>
window.ga=window.ga||function(){(ga.q=ga.q||[]).push(arguments)};ga.l=+new Date;
ga('create', '<?=getenv('GMETRIC_ID');?>', 'auto');
ga('send', 'pageview');
</script>
<script async src='https://www.google-analytics.com/analytics.js'></script>
<!-- End Google Analytics -->
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href="/resources/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script type="text/javascript">window.name = "Administrator panel"</script>
<script src="/resources/jquery-1.11.1.min.js"></script>
<script src="/resources/bootstrap.min.js"></script>
<style type=text/css>
@charset "utf-8";
body { margin:0; padding:0; width:100%; background:#f2f2f2;}
html { padding:0; margin:0;}
/* main */
.main { margin:0; padding:0;}
.main_resize { margin:0 auto; padding:0 10px; width:958px; border-right:1px solid #ececec; border-left:1px solid #ececec; border-bottom:1px solid #ececec; background:#fff;}
.main_resize h2 { font:normal 18px Arial, Helvetica, sans-serif; color:#2a2a2a; padding:10px 5px; margin:0;}
.main_resize h2 span { font:  italic 11px Arial, Helvetica, sans-serif; color:#b1b1b1;}
.main_resize h3 { font:normal 44px Arial, Helvetica, sans-serif; color:#7e7e7e; padding:20px 5px; margin:0; text-align: center;}
.main_resize p { font:normal 12px Arial, Helvetica, sans-serif; line-height:1.8em; padding:5px; margin:0;}
.main_resize p span { color:#b1b1b1; font: italic 11px Arial, Helvetica, sans-serif; line-height:1.8em;}
.main_resize a { color:#2d75c0; text-decoration:none;}
.main_resize img { margin:5px auto; padding:0;}
.main_resize ul.serv { list-style:none; margin:10px 0 10px 5px; padding:0; width:230px; float:left;}
.main_resize li.serv { padding:5px; margin:0; font:normal 12px Arial, Helvetica, sans-serif; color:#2d75c0;}
.main_resize img.floated { float:left; margin:5px 10px 5px 0; padding:0;}
.main_left { width:250px; float:left; padding:0 0 0 10px; margin:0;}
.main_right { width:680px; padding:0; margin:0 15%;}
.blog { width:280px; float:left; padding:0; margin:10px 30px 10px 10px;}
.blog2 { width:215px; float:left; padding:0; margin:10px 10px 30px 0;}
/* header */
.header { padding:0 0 10px 0; margin:0 auto; border-bottom:1px solid #dfdfdf;}
/* logo */
.logo {font: normal 36px Arial, Helvetica, sans-serif; color:#7e7e7e; padding:10px 20px 15px 0; margin:0; width:232px; float:left;}
/*.logo img { margin:0; padding:0;}*/
/*menu*/
.menu { width:555px; padding:10px 0 0 0; margin:0; float:left; }
.menu ul { padding:0; margin:0; list-style:none; border:0;}
.menu ul li { display:inline;  margin:0; padding:0; border:0; background:none;}
.menu ul li a {  display:inline; margin:0; padding:11px; color:#5a5a5a; font:normal 11px Arial, Helvetica, sans-serif; text-decoration:none;}
.menu ul li a:hover { background: #dfdfdf;}
.menu ul li a.active {  background: #dfdfdf;}
.loginform {font: normal 16px Arial, Helvetica, sans-serif; color:#7e7e7e; padding:15px; margin:0;}
.loginform .field { margin: 0px 0px 20px 10px }
#nav,#nav li ul{margin:0px;padding:0px;list-style:none;}

#nav > li{
	float: left;
}

#nav li ul{
	display:none;
	position:absolute;
	z-index:100;
}
#nav li:hover ul{display:block}


/*---------MAIN MENU------------*/
#nav > li{
	text-align:center;
}

#nav > li > a{
	text-decoration:none;
}

/*---------SUB MENU------------*/
#nav li ul{
	margin-top:10px;
	background-color:#ffffff;
	border:1px solid #E8E8E8;
}
#nav li ul li{
	text-align:left;
}
#nav li ul li a{
	color:#000000;
	text-decoration:none;
    display: block;
}
#nav li ul li a:hover{
	color:#553CCF;
	text-decoration:none;
}


 /* click */
.click { width:350px; float:right; padding:0; margin:0; text-align:right;}
.click p { font: normal 12px Arial, Helvetica, sans-serif; color:#959595; padding:0; margin:0;}
/* slider_main */
.slider_main { margin:0 auto; padding:0;}
.slider_main .leftt { float:left; width:252px; padding:0; margin:0;}
.slider_main .leftt h2 { font: normal 24px Arial, Helvetica, sans-serif; color:#616161; padding:71px 0 0 24px; margin:0; line-height:1.2em;}
.slider_main .leftt p { font: normal 12px Arial, Helvetica, sans-serif; color:#818181; padding:10px 0 15px 24px; margin:0; line-height:1.8em;}
.slider_main .leftt img { float:left; margin:0; padding:0 0 0 24px;}
/* search */
.search { padding:0; margin:0 auto 30px auto;}
.search form { display:block; float:left; padding:5px; margin:0; background:#fafafa;}
.search p { font: bold 11px Arial, Helvetica, sans-serif; color:#959595; padding:7px 0; height:16px; margin:0; float:left; line-height:14px;}
.search span { display:block; float:left; background:#f6f6f6; border:1px solid #eaeaea; width:150px; padding:0 5px; height:30px; margin:0 3px;}
.search form .keywords { line-height:16px; float:left; border:0; width:150px; padding:7px 0; height:16px; background:none; margin:0; font:normal 14px Arial, Helvetica, sans-serif; color:#9c9c9c;}
.search form .button { float:left; margin:0; padding:0;}
/* right_text */
.right_text { border-top:1px solid #dadada; background:#ececec; padding:0; margin:0 0 30px 0;}
.right_text p { font: normal 12px Arial, Helvetica, sans-serif; color:#7e7e7e; padding:15px; margin:0;}
/********** contact form **********/
#contactform { margin:0; padding:5px 10px; }
#contactform * { color:#F00; }
#contactform ol { margin:0; padding:0; list-style:none; }
#contactform li { margin:0; padding:0; background:none; border:none; display:block; clear:both; }
#contactform li.buttons { margin:5px 0 5px 0; clear:both; }
#contactform label { margin:0; width:150px; display:block; padding:10px 0; color:#222222; font: normal 12px Arial, Helvetica, sans-serif; text-transform:capitalize; float:left; }
#contactform label span { color:#F00; }
#contactform input.text { width:530px; border:1px solid #e8e8e8; margin:2px 0; padding:5px 2px; height:16px; background:#f8f8f8; float:left; }
#contactform textarea { width:530px; border:1px solid #e8e8e8; margin:2px 0; padding:2px; background:#f8f8f8; float:left; }
#contactform li.buttons input { padding:3px 0 3px 0px; margin:10px 0 0 0; border:0; color:#FFF; float:left; }
/*************footer**********/
.footer {padding:5px 0;  margin:0;}
.footer_resize { width:958px; margin:0 auto; padding:10px 10px; background:#202020;}
.footer p { font:normal 11px  Arial, Helvetica, sans-serif; color:#a2a2a2;}
.footer a { font:normal 11px Arial, Helvetica, sans-serif; color:#2d75c0; text-decoration:none; padding:5px; margin:0;}
.footer p.right { text-align:right; width:350px; margin:0; padding:8px 0 0 0; float:right;} 
.footer p.leftt { text-align:left; width:550px; margin:0; padding:8px 0 0 5px; float:left;} 

p.clr, .clr { clear:both; padding:0; margin:0; background:none;}
li.bg, .bg {  clear:both; border-top:1px solid #f2f2f2; width:100%; padding:0; margin:15px 0; background:none; line-height:0;}

</style>
<base href="/">
</head>
<body>
<div class="main">
  <div class="main_resize">
    <div class="header">
      <div class="logo"><a href="/">&nbsp;<font color=#2d75c0>Z</font><font color=#7e7e7e>Hack.Ru</font></a></div>
      <div class="clr"></div>
      <div class="menu">
        <ul id=nav>
          <li><a href="/"><span>Main</span></a></li>
          <li><a href="/contact/"><span>Contacts</span></a></li>
          <li><a id="logout" href="/admin/logout.php"><span>Logout</span></a></li>
        </ul>
      </div>
      <div class="clr"></div>
    </div>
    <div class="clr"></div>
    <h3>Administration panel</h3>
    <div class="main_left">
        <div class="clr"></div>
      </div>
    </div>
    <div class="main_right">
      <div class="right_text">
        <?php if ( isset($error) ){?>
          <p><strong>Error:</strong> <?php print($error);?></p>
        <?php } else { ?>
          <p><strong>Hint:</strong> flag is not a frag: once you've got it, you can get one more...</p>
        <?php } ?>
      </div>
      <div class="clr"></div>
      <?php if ( empty($_SESSION['userdata']) ) {?>
      <form method="POST" id="loginform" class="loginform">
        <div class="field">
          <label for="login">Login: </label>
          <input type="text" name="login"/>
        </div>
        <div class="field">
          <label for="password">Password: </label>
          <input type="password" name="password"/>
        </div>
        <button id="submit" type="submit">Submit</button>
      </form>
      <?php } else { ?>
        <div>
          <div class="container">
          <?php 
          if ($columns and $orders) { 
          ?>
          <form action="/admin/" id="orders" method="POST">
            <div class="row">
              <div id="no-more-tables">
                  <table class="col-md-12 table-bordered table-striped table-condensed cf">
                    <thead class="cf">
                      <tr>
                      <?php foreach ($columns as $name) {?>
                        <th><?=$name[0];?></th>
                      <?php } ?>
                        <th>select</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php 
                        foreach ($orders as $order) {
                      ?>
                        <tr>
                          <?php
                          foreach ($order as $propery) {
                            ?>
                            <td><?=$propery?></td>
                          <?php } ?>
                          <td><input type="checkbox" value="<?=$order['id'];?>" name="delete[]"/></td>  
                        </tr>
                      <?php } ?>
                    </tbody>
                  </table>
              </div>
            </div>
            <div class="row">
              <p class="bg-success" style="padding:10px;margin-top:20px"><small><button type="submit" id="submit">Mark</button> selecterd orders as read</small></p>
            </div>
          </form>
          <?php } else { ?>
            <div class="row">
              <p class="bg-success" style="padding:10px;margin-top:20px">There is no orders today :(</p>
            </div>
          <?php } ?>
          </div>
        </div>
      <? } ?>
    </div>
    <div class="clr"></div>
  </div>
  <div class="footer">
    <div class="footer_resize">
      <p class="leftt">&copy; 2001-2018. All Rights Reserved. <a href="/"><br />
        Main</a> | <a href="/contact/">Contact</a> </p>
      <p class="right">
      </p>
      <div class="clr"></div>
    </div>
    <div class="clr"></div>
  </div>
</div>
</body>
</html>
