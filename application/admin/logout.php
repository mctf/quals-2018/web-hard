<?php

@include('../config.php');

// Clear session data (only data column)
session_unset();

// Destroy the entire session
session_destroy();

header('Location: /');
