<?php

@include('../config.php');

if ( empty($_SESSION['userdata']) ){
	header('HTTP/1.1 404 Not Found', true, 404);
	echo "<html><head></head><body><h1>Not Found</h1><p>The requested URL /admin/source.php was not found on this server.</p><hr><address>Apache/2.4.25 (Debian) Server at localhost Port 8000</address></body></html>";
	exit();
}

#date_default_timezone_set("Russia/Moscow");
#header("Content-Type: text/event-stream\n\n");

// echo "event: message\n";
// $curDate = date(DATE_ISO8601);
// echo 'data: {"time": "' . $curDate . '", "text": "Hi. I must give you the secret key for our later conversation to encrypt our messages . But... Let\'s wait a minute, maybe someone get access to your computer. Watch for..."}';
// echo "\n\n";

// ob_end_flush();
// flush();
// sleep(3);

echo "event: message\n";
$curDate = date(DATE_ISO8601);
echo 'data: {"time": "' . $curDate . '", "text": "Hi. I must give you the secret key for our later conversation to encrypt our messages. The key is: ' . getenv('FLAG') . '"}';
echo "\n\n";

// ob_end_flush();
// flush();

// while (true) {
// 	sleep(100);
// }
?>
