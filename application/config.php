<?php

$flag = 0;

@ini_set("session.cookie_httponly", 1);
@ini_set('display_startup_errors', $flag);
@ini_set('display_errors', $flag);
@error_reporting(E_ALL);


// Define database configuration
define("DB_HOST", getenv("DB_HOST"));
define("DB_USER", "support");
define("DB_PASS", "haT3th1SsuPp0rT_n8fob*kg84");
define("DB_NAME", "zhack");

include("includes/database.class.php");
include("includes/mysql.sessions.php");

// @session_start();
$session = new Session();

try {

    // change localhost to 'db' for docker
    $dbh = new PDO('mysql:host='.DB_HOST.';dbname='.DB_NAME, DB_USER, DB_PASS);

} catch ( PDOException $e ) {

    exit('Database connection could not be established.');
}
