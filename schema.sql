CREATE DATABASE zhack;

USE zhack;

CREATE TABLE `orders` (
        `id` INT NOT NULL AUTO_INCREMENT,
        `title` TEXT NOT NULL,
        `text` TEXT NOT NULL,
        `name` TEXT NOT NULL,
        `email` TEXT NOT NULL,
        `site` TEXT NOT NULL,
        `date` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `sessions` (
        id varchar(32) NOT NULL,
        access int(10) unsigned,
        data text,
        PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `users` (
        `id` INT NOT NULL AUTO_INCREMENT,
        `name` TEXT NOT NULL,
        `uuid` CHAR(255) UNIQUE NOT NULL,
        `login` CHAR(255) UNIQUE NOT NULL,
        `password` CHAR(255) NOT NULL,
        `status` CHAR(128) DEFAULT 'default', -- default status or premium
        `type` CHAR(128) DEFAULT 'default', -- default user or support
        `date` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE USER 'support'@'%' IDENTIFIED BY 'haT3th1SsuPp0rT_n8fob*kg84';
CREATE USER 'bot'@'%' IDENTIFIED BY 'G*!OGDIBKAJSGD*&!@Pgkasdgl';

GRANT INSERT,SELECT,UPDATE,DELETE ON `zhack`.`sessions` TO 'support'@'%';
GRANT INSERT,SELECT,UPDATE,DELETE ON `zhack`.`orders` TO 'support'@'%';
GRANT INSERT,SELECT,UPDATE ON `zhack`.`users` TO 'support'@'%';
GRANT SELECT,UPDATE ON `zhack`.* TO 'bot'@'%';

FLUSH PRIVILEGES;


-- support password kohXaer1raeWei!phee:pai1jaeb,aen0
INSERT INTO users (name, uuid, login, password, status, type) VALUES ('Supp0rt', '7828a146-6dee-4261-8a43-a2c9f1719ed2', 'support', '$2y$10$l.HiOLra0l4y2IKax5lqCuiaDRrvThG.0pPpTfRKMJdARj7bd/gkK', 'default', 'support');

-- orders table
INSERT INTO orders (title, text, name, email, site) VALUES ('Magic Thumb', 'Magic Thumbs Store', 'Leo', 'admin@store.com', 'MagicThimbsStore.com');
INSERT INTO orders (title, text, name, email, site) VALUES ('SomePentest', 'Magic Thumbs Store', 'Leo', 'admin@store.com', 'yandex.com');
INSERT INTO orders (title, text, name, email, site) VALUES ('AnySite   ', 'Magic Thumbs Store', 'Leo', 'admin@store.com', 'google.com');
INSERT INTO orders (title, text, name, email, site) VALUES ('CoolBro', 'Magic Thumbs Store', 'Leo', 'admin@store.com', 'bing.com');
INSERT INTO orders (title, text, name, email, site) VALUES ('VeryFan', 'Magic Thumbs Store', 'Leo', 'admin@store.com', 'yahoo.com');
INSERT INTO orders (title, text, name, email, site) VALUES ('ANything', 'Magic Thumbs Store', 'Leo', 'admin@store.com', 'rabmler.com');
INSERT INTO orders (title, text, name, email, site) VALUES ('SomeShit', 'Magic Thumbs Store', 'Leo', 'admin@store.com', 'shodan.com');
INSERT INTO orders (title, text, name, email, site) VALUES ('TooHard', 'Magic Thumbs Store', 'Leo', 'admin@store.com', 'censys.com');
INSERT INTO orders (title, text, name, email, site) VALUES ('EzBro', 'Magic Thumbs Store', 'Leo', 'admin@store.com', 'riskyq.com');
INSERT INTO orders (title, text, name, email, site) VALUES ('AnotherShit', 'Magic Thumbs Store', 'Leo', 'admin@store.com', 'nigma.com');
INSERT INTO orders (title, text, name, email, site) VALUES ('MakePentest', 'Magic Thumbs Store', 'Leo', 'admin@store.com', 'mathlab.com');
INSERT INTO orders (title, text, name, email, site) VALUES ('LetsFun', 'Magic Thumbs Store', 'Leo', 'admin@store.com', 'wolfram.com');